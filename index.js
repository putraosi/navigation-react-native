/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import App2 from './App_2';
import App3 from './App_3';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
