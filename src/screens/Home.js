import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button
} from 'react-native';

export default class Home extends Component{
    render(){
        return(
            <View>
                <Text>Home Screen</Text>
                <Button
                    title="Go to Main Screen"
                    onPress={()=>this.props.navigation.navigate("Main")}/>
            </View>
        )
    }
}