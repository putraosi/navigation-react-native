import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button
} from 'react-native';

import { createStackNavigator } from 'react-navigation';

import FirstScreen from './First';
import SecondScreen from './Second';
import Routes from './Routes';


const Taps = createStackNavigator({
    Pertama: {
        screen: FirstScreen,
        navigationOptions:{
            header: null
        }
    },
    Kedua: {
        screen: SecondScreen,
        navigationOptions:{
            tabBarVisible: false
        }
    }
});

Taps.navigationOptions = ({ navigation }) => {
    let { routeName } = navigation.state.routes[navigation.state.index];
    let navigationOptions = {};
  
    if (routeName === 'Kedua') {
      navigationOptions.tabBarVisible = false;
      navigationOptions.swipeEnabled = false;
    }
  
    return navigationOptions;
  };

export default Taps;