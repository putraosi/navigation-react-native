import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button
} from 'react-native';

export default class Second extends Component{

    render(){
        return(
            <View>
                <Text>Second Screen</Text>
            </View>
        )
    }
}