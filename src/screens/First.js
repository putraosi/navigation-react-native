import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button
} from 'react-native';

export default class First extends Component{

    render(){
        return(
            <View>
                <Text>First Screen</Text>
                <Button
                    title="Go to Second Screen"
                    onPress={()=>this.props.navigation.navigate("Kedua")}/>
            </View>
        )
    }
}