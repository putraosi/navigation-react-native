import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import Example1 from './Example1'
import Example2 from './Example2'

const Routes = () => (
   <Router>
      <Scene key = "root">
         <Scene key = "home" component = {Example1} title = "Home" initial = {true} />
         <Scene key = "about" component = {Example2} title = "About" />
      </Scene>
   </Router>
)
export default Routes