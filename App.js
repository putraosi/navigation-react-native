/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

import { createStackNavigator, createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

// SCREENS
import HomeScreen from './src/screens/Home';
import MainScreen from './src/screens/Main';
import SettingsScreen from './src/screens/Settings';
import ProfileScreen from './src/screens/Profile';
import Tabs1Screen from './src/screens/Tab1';
import Tabs2Screen from './src/screens/Tab2';

const Stack1 = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      title: 'Home',
    },
  },
  Main: {
    screen: MainScreen,
    navigationOptions: {
      title: 'Main',
    },
  },
});

Stack1.navigationOptions = ({ navigation }) => {
  let { routeName } = navigation.state.routes[navigation.state.index];
  let navigationOptions = {};

  if (routeName === 'Main') {
    navigationOptions.tabBarVisible = false;
  }

  return navigationOptions;
};

const tabBar = createMaterialTopTabNavigator({
  First: {
    screen: Tabs1Screen,
  },
  Second: {
    screen: Tabs2Screen,
  }
})

const Tabs = createBottomTabNavigator(
  {
    Home: {
      screen: Stack1,
      navigationOptions: {
        title: 'Home',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-home" color={tintColor} size={24} />
        )
      },
    },
    Settings: {
      screen: tabBar,
      navigationOptions: {
        title: 'Settings',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-settings" color={tintColor} size={24} />
        )
      },
    },

    Profile: {
      screen: ProfileScreen,
      navigationOptions: {
        title: 'Profile',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-person" color={tintColor} size={24} />
        ),
        tabBarVisible: false
      },
    },
  },
  {}
);

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Tabs />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
});
