/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    SafeAreaView
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import HomeScreen from './src/screens/Home';
import MainScreen from './src/screens/Main';

const Navigation = StackNavigator({
    Home: {
        screen: HomeScreen
    },
    Main: {
        screen: MainScreen,
        navigationOptions: {
            title: 'Main',
            header: null,
        }
    }
})

export default Navigation;